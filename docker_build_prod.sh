#!/usr/bin/env bash

cp backend/.env.prod backend/.env
docker-compose -f docker-compose-prod.yml build