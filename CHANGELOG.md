# Changelog
All notable changes to this project will be documented in this file.

## [1.0.0] - 2018-09-22
### Added
- .babelrc configuration file
- .env(dev|stage|prod) configuration files
- .eslintrc configuration file
- Dockerfile for deploying backend
- Gruntfile.js configuration file
- Docker scripts for build and starting backend and database
- Docker compose files for each deployment stage
- Script that waits for services
- Directory for frontend
- note to README.md file

### Changed

### Removed
