const devWebpackConfig = require('./webpack.dev');
const prodWebpackConfig = require('./webpack.prod');

module.exports = function(grunt) {
    grunt.initConfig({
        webpack: {
            options: {
                stats:
                    !process.env.NODE_ENV ||
                    process.env.NODE_ENV === 'development'
            },
            dev: devWebpackConfig,
            prod: prodWebpackConfig
        },
        eslint: {
            options: {
                configFile: '.eslintrc'
            },
            target: ['src/**/*.js']
        },
        mocha: {
            src: ['tests/**/*.html'],
        }
    });
    grunt.loadNpmTasks('grunt-webpack');
    grunt.loadNpmTasks('grunt-eslint');
    grunt.loadNpmTasks('grunt-mocha');

    grunt.registerTask('default', ['webpack:dev','webpack:prod']);
    grunt.registerTask('default', ['eslint']);
    grunt.registerTask('default', ['mocha']);
};
