#!/usr/bin/env bash

cp backend/.env.dev backend/.env
docker-compose -f docker-compose-dev.yml build
