#!/usr/bin/env bash

#set -x # debug on

STATUS_BACKEND=
STATUS_FRONTEND=

PORT_BACKEND=8080
PORT_FRONTEND=8081

echo "Waiting for backend service..."
while [ -a ${STATUS_BACKEND} ]; do
    read STATUS_BACKEND < <(
        curl -sI http://localhost:${PORT_BACKEND} |
        awk '/^HTTP/ { STATUS_BACKEND = $2 }
             END { printf("%s\n",STATUS_BACKEND) }'
    )
    printf '.'
    sleep 1
done
echo "Backend service online."

echo "Waiting for frontend service..."
while [ -a ${STATUS_FRONTEND} ]; do
    read STATUS_FRONTEND < <(
        curl -sI http://localhost:${PORT_FRONTEND} |
        awk '/^HTTP/ { STATUS_FRONTEND = $2 }
             END { printf("%s\n",STATUS_FRONTEND) }'
    )
    printf '.'
    sleep 1
done
echo "Frontend service online."
